# visualisation_fractions

Page web permettant de visualiser des fractions dans le navigateur.

## Version en ligne

La page est accessible à cette adresse : [https://nreveret.forge.apps.education.fr/visualisation_fractions](https://nreveret.forge.apps.education.fr/visualisation_fractions).

## Téléchargement

Télécharger le fichier `index.html` et l'ouvrir dans un navigateur.

Une connexion internet est nécessaire afin de charger les modules complémentaires (`bulma` , `jQuery` et `MathJax`).
